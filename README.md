## Available Scripts

In the project directory, you can run:
- `npm i` or `yarn` to install project dependences.
- `npm run dev` or `yarn dev` to run the project in development mode. It also watch project codes for changes and relaod the app in browser
- `npm run build` or `yarn build` to build the project and minify sources for production into **build** folder

### `npm i --dev parcel@next` or `yarn add --dev parcel@next`


## Starting the server

To run the app in the development mode, run `yarn dev` then:
Open [http://localhost:1234](http://localhost:1234) to view it in the browser.

The page will reload if you make edits.<br>
You will also see any lint errors in the console.

### `npm test` or `yarn test`

## Learn More

You can learn more in the [Parcel documentation](https://parceljs.org/).
