const express = require('express')
const app = express()
const path = require('path')
const compression = require('compression')

// Set Express middlewares
app.use(compression())
app.use(express.static(__dirname + '/build', {
  maxAge: '30 days'
}))

app.get('/*', function (req, res) {
  res.sendFile(path.join(__dirname + '/build/index.html'))
})

// Start the app by listening on the default
// Heroku port
app.listen(process.env.PORT || 8080)

console.log('App served on port: ', process.env.PORT || 8080)
