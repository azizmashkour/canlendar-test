export const Config = {
  apiScheme: 'https',
  apiDomain: 'reservationcalendar.herokuapp.com',
  toApiUrl(path = '') {
    return `${this.apiScheme}://${this.apiDomain}/reserve/${path}`
  }
}
