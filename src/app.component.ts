import { IComponentOptions } from 'angular';

import template from './app.component.html';

export default class AppComponent implements IComponentOptions {
  static template = template
}
