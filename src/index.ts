import { module } from 'angular';
import AppComponent from './app.component'
import { AppComponentsModule } from './components'
import { AppServicesModule } from './services';

import '../scss/style.scss'

module('appModule',
  [AppComponentsModule, AppServicesModule]
)
  .component('app', AppComponent)
