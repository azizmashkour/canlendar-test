import { Moment, unix } from 'moment-timezone'

export default class Tenant {
  name: string
  time: number
  parsedTime: Moment
  private locale = 'Asia/Dubai'

  constructor(data) {
    this.name = data.tennantName
    this.time = data.time
    this.parsedTime = unix(data.time).tz(this.locale).startOf('day')
  }

  static fromArray(data: Array<any>) {
    return data.map(item => new Tenant(item))
  }

  public get formatedTime() {
    return this.parsedTime.format('MMM D')
  }

}
