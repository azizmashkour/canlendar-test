import { module } from 'angular'
import { CalendarService } from './calendar-service/calendar-service'

export const AppServicesModule = module('servicesModule', [])
  .service('calendarService', CalendarService)
  .name
