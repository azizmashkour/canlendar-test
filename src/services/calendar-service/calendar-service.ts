import Moment from 'moment-timezone';
import { extendMoment } from 'moment-range';
import { IHttpService } from 'angular';
import Tenant from '../../models/tenant';
import { Config } from '../../app.config';

Moment.locale('en')
const moment = extendMoment(Moment)

export interface ITenantsResponse {
  reserved: Array<ITenant>
}

export interface ITenant {
  tennantName: string
  time: number
}

export class CalendarService {
  start: Moment.Moment
  end: Moment.Moment
  currentDate: Moment.Moment;
  tenants: Array<Tenant> = [];
  locale = 'Asia/Dubai'

  static $inject = ['$http']

  constructor(private http: IHttpService) {
    this.currentDate = moment()
    this.start = moment([this.currentDate.year(), this.currentDate.month()])
    this.end = this.start.clone().endOf('month')
  }

  getWeekStart() {
    return this.start.weekday()
  }

  getDays() {
    const range = moment.range(this.start, this.end)

    return Array.from(range.by('days'))
  }

  next() {
    this.currentDate.add(1, 'months')
    this.end.add(1, 'months')
    this.start.add(1, 'months')
    this.loadTenants()
  }

  previous() {
    this.currentDate.subtract(1, 'months')
    this.start.subtract(1, 'months')
    this.end.subtract(1, 'months')
    this.loadTenants()
  }

  findTenants(date: string): Array<Tenant> {
    return this.tenants.filter((item) => item.formatedTime === date)
  }

  setCurrentDay(day: Moment.Moment) {
    this.currentDate = day
  }

  loadTenants() {
    const from = this.start.unix()
    const to = this.end.unix()

    return this.http.get<ITenantsResponse>(this.apiUrl(`${from}/${to}`))
      .then(resp => {
        if (resp.data.reserved.length) {
          this.tenants = Tenant.fromArray(resp.data.reserved)
        }
      })
  }

  cancelStay(tenant: Tenant) {
    return this.http.post(this.apiUrl(), {
      tennantName: tenant.name,
      time: tenant.time,
      reserved: false
    }).then(response => {
      this.tenants.splice(this.tenants.indexOf(tenant), 1)
      return response
    })
  }

  addStay(tenant: Tenant) {
    return this.http.post(this.apiUrl(), {
      tennantName: tenant.name,
      time: tenant.time,
      reserved: true
    }).then(response => {
      this.tenants.push(tenant)
      return response
    })
  }

  private apiUrl(segments = '') {
    return Config.toApiUrl(segments)
  }
}
