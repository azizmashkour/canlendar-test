import { IComponentOptions, IComponentController } from 'angular';
import { CalendarService } from '../../services';

import template from './calendrier.component.html'
import { Moment } from 'moment';

export class CalendarController implements IComponentController {
  days = ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat']
  monthDays = []

  static $inject = ['calendarService']

  constructor(private month: CalendarService) {
  }

  $onInit() {
    this.getMonthDays()
    this.month.loadTenants()
  }

  public get year() {
    return this.month.currentDate.format('MMM YYYY')
  }


  public get currentDate(): Moment {
    return this.month.currentDate
  }


  public get WeekStart() {
    return this.month.getWeekStart()
  }

  public get spacerWidth() {
    return {
      width: `calc((100% / 7) * ${this.WeekStart}`
    }
  }

  public nextMonth() {
    this.month.next()
    this.getMonthDays()
  }

  public previousMonth() {
    this.month.previous()
    this.getMonthDays()
  }

  public selectDay(day: Moment) {
    this.month.setCurrentDay(day)
  }

  private getMonthDays() {
    this.monthDays = this.month.getDays();
  }

}

export const CalendarComponent: IComponentOptions = {
  controller: CalendarController,
  controllerAs: 'vm',
  template,
}
