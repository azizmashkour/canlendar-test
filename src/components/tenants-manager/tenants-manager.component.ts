import { IComponentController, IComponentOptions, IFormController } from 'angular'
import { CalendarService } from '../../services'

import template from './tenants-manager.component.html'
import Tenant from '../../models/tenant'

export class TenantsManagerController implements IComponentController {
  static $inject = ['calendarService']

  tennantName = ''

  constructor(private month: CalendarService) {
  }


  public get selectedDay() {
    return this.month.currentDate.format('MMM D')
  }


  public get tenants() {
    return this.month.findTenants(this.selectedDay)
  }

  cancelStay(tenant: Tenant) {
    this.month.cancelStay(tenant)
  }

  confirmStay(form: IFormController) {
    if (form.$invalid) {
      return
    }

    const tenant = new Tenant({
      tennantName: this.tennantName,
      time: this.month.currentDate.tz(this.month.locale).startOf('day').unix(),
      reserved: true
    })

    this.month.addStay(tenant).then((resp) => {
      this.tennantName = ''
    }, (error) => {
      alert(error.data)
    })
  }
}

export const TenantsManagerComponent: IComponentOptions = {
  controller: TenantsManagerController,
  controllerAs: 'vm',
  template
}
