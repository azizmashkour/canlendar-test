import { module } from 'angular'
import { CalendarComponent } from './calendar/calendar.component'
import { TenantsManagerComponent } from './tenants-manager/tenants-manager.component';

export const AppComponentsModule = module('componentsModule', [])
  .component('calendar', CalendarComponent)
  .component('tenantsManager', TenantsManagerComponent)
  .name;
